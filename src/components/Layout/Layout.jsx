import React from 'react';
import Container from '@material-ui/core/Container';
import Navbar from './Navbar';

const Layout = (props) => (
  <>
    <Navbar />
    <Container>
      {props.children}
    </Container>
  </>
);

export default Layout;

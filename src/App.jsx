import React, { Component } from 'react';
import './App.css';
import {
  Redirect, Route, Switch, withRouter,
} from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Layout from './components/Layout/Layout';
import About from './components/Views/About';
import Home from './components/Views/Home';
import Login from './components/Views/Login';
import { authCheckState } from './store/actions/index';

class App extends Component {
  componentDidMount() {
    this.props.onTryAutoLogin();
  }

  render() {
    return (
      <>
        <Switch>
          <Route path="/login" component={Login} />
          <Layout>
            <Switch>
              <Route path="/about" component={About} />
              <Route path="/" component={Home} />
            </Switch>
          </Layout>
          <Redirect to="/" />
        </Switch>
      </>
    );
  }
}

App.propTypes = {
  onTryAutoLogin: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.token !== null,
});

const mapDispatchToProps = (dispatch) => ({
  onTryAutoLogin: () => dispatch(authCheckState()),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
